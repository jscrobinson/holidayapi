<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests;
use App\Repositories\HolidaysRepository;
use App\Exceptions\HolidaysRepositoryException;
use Symfony\Component\Routing\Exception\InvalidParameterException;

class HolidayController extends Controller {

	/**
	 * Parameter whitelist
	 * 
	 * @var array
	 */
	protected $valid_parameters = [
		'year',
		'country',
		'month',
		'day',
		'previous',
		'upcoming',
	];

	/**
	 *
	 * @var Response 
	 */
	protected $response;

	/**
	 * Pretty print json response
	 * 
	 * @var boolean 
	 */
	protected $pretty = false;

	/**
	 * 
	 * @param Response $response
	 * @param Request $request
	 */
	public function __construct( Request $request, Response $response ) {
		if ( $request->get( 'pretty' ) !== null ) {
			$this->pretty = true;
		}
		$this->response = $response;
		$this->response->header( 'Content-Type', 'text/json' );
	}

	/**
	 * Get holiday data
	 *
	 * @param  Request  $request
	 * @param HolidaysRepository $holidayRepository
	 * @return Response
	 */
	public function getAction( Request $request, HolidaysRepository $holidayRepository ) {

		try {
			$holidayData = $holidayRepository->getHolidays( $this->getValidParameters( $request ) );
			$this->sendSuccessResponse( $holidayData );
		} catch ( InvalidParameterException $ex ) {
			$this->sendErrorResponse( $ex->getMessage(), 400 );
		}
		catch ( HolidaysRepositoryException $ex ) {
			$this->sendErrorResponse( $ex->getMessage(), 400 );
		}
		
	}

	/**
	 * 
	 * @param Request $request
	 * @return boolean
	 */
	public function getValidParameters( Request $request ) {
		$parameters = [ ];
		foreach ( $this->valid_parameters AS $key ) {
			$val	 = $request->get( $key );
			$error	 = false;
			switch ( $key ) {
				case 'day':
				case 'month':
					if ( !is_null( $val ) && !preg_match( '@^([0-9]{1,2})$@', $val ) ) {
						$error = 'The ' . $key . ' parameter is not valid.';
					}
					break;
				case 'country':
					if ( is_null( $val ) ) {
						$error = 'The country parameter is required.';
					}
					if ( !preg_match( '@^([A-Z]{2})$@', $val ) ) {
						$error = 'The '.$key.' parameter is not valid.';
					}
					break;
				case 'year':
					
					if ( is_null( $val ) ) {
						$error = 'The year parameter is required.';
					} elseif ( !preg_match( '@^([0-9]{4})$@', $val ) ) {
						$error = 'The '.$key.' parameter is not valid.';
					}
					break;
				case 'previous':
				case 'upcoming':
					if ( is_null( $val ) ) {
						continue 2;
					} else {
						$val = true;
					}
					break;
				default:
					if ( is_null( $val ) ) {
						continue 2;
					}
					break;
			}
			if($error) {				
				throw new InvalidParameterException($error);				
			}
			$parameters[ $key ] = $val;
		}
		return $parameters;
		
	}

	public function sendErrorResponse( $message, $code ) {
		$this->response->setStatusCode( $code );
		$this->sendJsonResponse( [
			'status' => $code,
			'error'	 => $message,
		] );
		return false;
	}

	public function sendSuccessResponse( $data, $code = 200 ) {
		$this->response->setStatusCode( $code, 'Success' );
		$returnArray = [
			'status' => $code,
		];
		$returnArray = array_merge( $returnArray, $data );
		$this->sendJsonResponse( $returnArray );
	}

	public function sendJsonResponse( $array ) {
		if ( $this->pretty ) {
			$json = json_encode( $array, JSON_PRETTY_PRINT );
		} else {
			$json = json_encode( $array );
		}

		$this->response->setContent( $json );
		$this->response->send();
	}

}
