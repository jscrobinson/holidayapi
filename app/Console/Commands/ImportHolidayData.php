<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use App\Holiday AS HolidayModel;

class ImportHolidayData extends Command {

	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'holiday:import 
							{filepath : The path to an individual json file or a folder containing multiple json files for a bulk import}';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Import holiday data from JSON file(s)';

	/**
	 * The number of files imported
	 * 
	 * @var int
	 */
	protected $fileCount = 0;

	/**
	 * The number of holiday rows created
	 * 
	 * @var int 
	 */
	protected $newHolidayCount = 0;
	
	/**
	 * The number of holiday rows updated
	 * 
	 * @var int 
	 */
	protected $updatedHolidayCount = 0;

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct() {
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle() {		
		$filePath = $this->argument( 'filepath' );
		try {
			$this->doImport( $filePath );
		} catch ( \Exception $ex ) {
			$this->error( $ex->getMessage() );
		}
	}

	/**
	 * 
	 * @param string $filePath
	 * @throws Exception
	 */
	public function doImport( $filePath ) {
		if ( !File::exists( $filePath ) ) {
			throw new \Exception( 'Specified path does not exist or cannot be read' );
		}

		if ( File::isDirectory( $filePath ) ) {
			$this->doBulkImport( $filePath );
		} else {
			$this->doSingleImport( $filePath );
		}
		$this->info('Imported '.$this->fileCount.' files');
		$this->info('Imported '.$this->newHolidayCount.' new holiday records');
		$this->info('Imported '.$this->updatedHolidayCount.' updated holiday records');
		$this->info('Holiday data imported successfully');
	}

	/**
	 * 
	 * @param string $filePath
	 */
	public function doBulkImport( $filePath ) {
		foreach ( File::allFiles( $filePath ) AS $singleFile ) {
			if ( $singleFile->isFile() && $singleFile->getExtension() == 'json' ) {
				$this->doSingleImport( $singleFile->getPathname() );
				$this->fileCount++;
			}
		}
	}

	/**
	 * Get the data as an array from the json file.
	 * Get the new model or update an existing one.
	 * Update counters
	 * 
	 * @param string $filePath
	 */
	public function doSingleImport( $filePath ) {
		if ( $this->validateJsonFile( $filePath ) ) {
			$this->info('Importing "' . $filePath . '"');
			$data = json_decode( File::get( $filePath ), true );
			$country = $this->getCountryCodeFromFilename($filePath);
			foreach ($data AS $holidayRow) {					
				$holidayRowObj = HolidayModel::create(['country' => $country]);				
				$holidayRowObj->fill($holidayRow);			
				$exists = $holidayRowObj->exists;				
				if ($holidayRowObj->save()) {
					if($holidayRowObj->wasRecentlyCreated) {
						$this->newHolidayCount++;
					} else {
						$this->updatedHolidayCount++;
					}
				}
			}			
		} else {
			throw new \Exception( 'Invalid JSON file "' . $filePath . '"' );
		}
	}
	
	/**
	 * Get the country code from the file path.
	 * 
	 * @param string $filePath
	 * @return string
	 * @throws \Exception
	 */
	public function getCountryCodeFromFilename( $filePath ) {
		$matched = preg_match('@^([A-Z]+).*@', basename($filePath), $matches);
		if(!$matched || empty($matches[1])) {
			throw new \Exception('Failed to get country code from filename "' . $filePath . '"');
		}
		return $matches[1];
	}

	/**
	 * Validate json file
	 * 
	 * @param type $filePath
	 * @return boolean
	 */
	public function validateJsonFile( $filePath ) {
		if ( File::exists( $filePath ) && File::extension( $filePath ) == 'json' ) {
			return true;
		}
		return false;
	}

}
