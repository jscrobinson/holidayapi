<?php

namespace App\Repositories;

use App\Exceptions\HolidaysRepositoryException;
use App\Holiday;

class HolidaysRepository {

	/**
	 * Get parsed holiday array
	 * 
	 * @param type $parameters
	 * @return type
	 * @throws HolidaysRepositoryException
	 */
	public function getHolidays( $parameters ) {
		$holidays = [ ];
		$year		 = $parameters[ 'year' ];
		$month		 = isset( $parameters[ 'month' ] ) ? str_pad( $parameters[ 'month' ], 2, '0', STR_PAD_LEFT ) : '';
		$day		 = isset( $parameters[ 'day' ] ) ? str_pad( $parameters[ 'day' ], 2, '0', STR_PAD_LEFT ) : '';
		$country	 = isset( $parameters[ 'country' ] ) ? strtoupper( $parameters[ 'country' ] ) : '';
		$previous	 = isset( $parameters[ 'previous' ] );
		$upcoming	 = isset( $parameters[ 'upcoming' ] );
		$date		 = $year . '-' . $month . '-' . $day;

		if ( $previous && $upcoming ) {
			throw new HolidaysRepositoryException( 'You cannot request both previous and upcoming holidays.' );
		} elseif ( ($previous || $upcoming) && (!$month || !$day) ) {
			$request = $previous ? 'previous' : 'upcoming';
			$missing = !$month ? 'month' : 'day';

			throw new HolidaysRepositoryException( 'The ' . $missing . ' parameter is required when requesting ' . $request . ' holidays.' );
		}

		if ( $month && $day ) {
			if ( strtotime( $date ) === false ) {
				throw new HolidaysRepositoryException( 'The supplied date (' . $date . ') is invalid.' );
			}
		}

		$country_holidays = $this->calculateHolidays( $country, $year, $previous || $upcoming );		


		$payload[ 'holidays' ] = [ ];

		if ( $month && $day ) {
			if ( $previous ) {
				$country_holidays		 = $this->flatten( $date, $country_holidays[ $year - 1 ], $country_holidays[ $year ] );
				prev( $country_holidays );
				$payload[ 'holidays' ]	 = current( $country_holidays );
			} elseif ( $upcoming ) {
				$country_holidays		 = $this->flatten( $date, $country_holidays[ $year ], $country_holidays[ $year + 1 ] );
				next( $country_holidays );
				$payload[ 'holidays' ]	 = current( $country_holidays );
			} elseif ( isset( $country_holidays[ $year ][ $date ] ) ) {
				$payload[ 'holidays' ] = $country_holidays[ $year ][ $date ];
			}
		} elseif ( $month ) {
			foreach ( $country_holidays[ $year ] as $date => $country_holiday ) {
				if ( substr( $date, 0, 7 ) == $year . '-' . $month ) {
					$payload[ 'holidays' ] = array_merge( $payload[ 'holidays' ], $country_holiday );
				}
			}
		} else {
			$payload[ 'holidays' ] = $country_holidays[ $year ];
		}


		return $payload;
	}

	/**
	 * Get holiday data from database and parse rules
	 * @param string $country
	 * @param int $year
	 * @param boolean $range
	 * @return array
	 */
	protected function calculateHolidays( $country, $year, $range = false ) {
		$userQuery = Holiday::where( 'country', $country );

		$country_holidays = $userQuery->get()->toArray();

		$return = [ ];

		if ( $range ) {
			$years = [$year - 1, $year, $year + 1 ];
		} else {
			$years = [$year ];
		}
		foreach ( $years as $year ) {
			foreach ( $country_holidays as $country_holiday ) {
				if ( strstr( $country_holiday[ 'rule' ], '%Y' ) ) {
					$rule = str_replace( '%Y', $year, $country_holiday[ 'rule' ] );
				} elseif ( strstr( $country_holiday[ 'rule' ], '%EASTER' ) ) {
					$rule = str_replace( '%EASTER', date( 'Y-m-d', strtotime( $year . '-03-21 +' . easter_days( $year ) . ' days' ) ), $country_holiday[ 'rule' ] );
				} elseif ( in_array( $country, ['BR', 'US' ] ) && strstr( $country_holiday[ 'rule' ], '%ELECTION' ) ) {
					switch ( $country ) {
						case 'BR':
							$years	 = range( 2014, $year, 2 );
							break;
						case 'US':
							$years	 = range( 1788, $year, 4 );
							break;
					}

					if ( in_array( $year, $years ) ) {
						$rule = str_replace( '%ELECTION', $year, $country_holiday[ 'rule' ] );
					} else {
						$rule = false;
					}
				} else {
					$rule = $country_holiday[ 'rule' ] . ' ' . $year;
				}

				if ( $rule ) {
					$calculated_date = date( 'Y-m-d', strtotime( $rule ) );

					if ( !isset( $calculated_holidays[ $calculated_date ] ) ) {
						$calculated_holidays[ $calculated_date ] = [ ];
					}

					$calculated_holidays[ $calculated_date ][] = [
						'name'		 => $country_holiday[ 'name' ],
						'country'	 => $country,
						'date'		 => $calculated_date,
					];
				}
			}

			$country_holidays = $calculated_holidays;

			ksort( $country_holidays );

			foreach ( $country_holidays as $date_key => $date_holidays ) {
				usort( $date_holidays, function($a, $b) {
					$a	 = $a[ 'name' ];
					$b	 = $b[ 'name' ];

					if ( $a == $b ) {
						return 0;
					}

					return $a < $b ? -1 : 1;
				} );

				$country_holidays[ $date_key ] = $date_holidays;
			}

			$return[ $year ] = $country_holidays;
		}
		return $return;
	}

	/**
	 * 
	 * @param type $date
	 * @param type $array1
	 * @param type $array2
	 * @return boolean
	 */
	private function flatten( $date, $array1, $array2 ) {
		$holidays = array_merge( $array1, $array2 );

		// Injects the current date as a placeholder
		if ( !isset( $holidays[ $date ] ) ) {
			$holidays[ $date ] = false;
			ksort( $holidays );
		}

		// Sets the internal pointer to today
		while ( key( $holidays ) !== $date ) {
			next( $holidays );
		}

		return $holidays;
	}

}
