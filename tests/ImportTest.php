<?php
use Mockery;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Console\Commands\ImportHolidayData;
use Symfony\Component\Console\Tester\CommandTester;

class Import extends TestCase {

	protected $testData = [
		[
			'name'	 => 'New Year\'s Day',
			'rule'	 => 'January 1st',
		],
		[
			'name'	 => 'Easter Monday',
			'rule'	 => '%EASTER +1 day',
		],
		[
			'name'	 => 'Good Friday',
			'rule'	 => '%EASTER -2 days',
		],
		[
			'name'	 => 'Valentine\'s Day',
			'rule'	 => 'February 14th',
		],
		[
			'name'	 => 'Saint Patrick\'s Day',
			'rule'	 => 'March 17th',
		],
		[
			'name'	 => 'April Fools\' Day',
			'rule'	 => 'April 1st',
		],
		[
			'name'	 => 'Early May Bank Holiday',
			'rule'	 => 'First Monday of May',
		],
		[
			'name'	 => 'Spring Bank Holiday',
			'rule'	 => 'Last Monday of May',
		],
		[
			'name'	 => 'Fathers Day',
			'rule'	 => 'Third Sunday of June',
		],
		[
			'name'	 => 'Summer Bank Holiday',
			'rule'	 => 'Last Monday of August',
		],
		[
			'name'	 => 'Christmas (possibly in lieu)',
			'rule'	 => '24 December %Y +1 weekday',
		],
		[
			'name'	 => 'Boxing Day (possibly in lieu)',
			'rule'	 => '24 December %Y +2 weekday',
		],
	];
	protected $command;
	protected $store;

	/**
	 * Set up command for testing
	 */
	protected function setUp() {
		parent::setUp();		
		$this->command	 = new ImportHolidayData( );		
		$this->command->setLaravel( $this->app->make( 'Illuminate\Contracts\Foundation\Application' ) );
		$this->command->setApplication( $this->app->make( 'Symfony\Component\Console\Application' ) );
		if ( !File::exists( storage_path() . '/holidaydata' ) ) {
			File::makeDirectory( storage_path() . '/holidaydata' );
		}
		File::put( storage_path() . '/holidaydata/GB.json', json_encode( $this->testData ) );
		DB::beginTransaction();
	}

	/**
	 * Test import command
	 *
	 * @return void
	 */
	public function testJsonFileImport() {
		
		$commandTester = new CommandTester($this->command);
		$result = $commandTester->execute( [			
			'filepath'	 => storage_path() . '/holidaydata/GB.json',
		] );

		$this->assertSame(0, $result);
		
		$this->assertRegExp( '@Holiday data imported successfully@', $commandTester->getDisplay() );
		
	}

	/**
	 * Runs after each test.
	 */
	public function tearDown() {
		DB::rollback();
		
		File::delete( storage_path() . '/holidaydata/GB.json' );
	}

}
