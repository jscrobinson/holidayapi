var elixir = require('laravel-elixir');
var gulp    = require('gulp');
var connect = require('gulp-connect-php');
var sync    = require('browser-sync');
/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix.sass('app.scss');
});

gulp.task('server', function() {

    connect.server({ base: './public', port: 8080 }, function() {
      sync({ proxy: 'localhost:8080' });
    });

    gulp.watch('public/css/styles.css', ['styles']);

    gulp.watch('**/*.php').on('change', function() {
      sync.reload();
    });
});
